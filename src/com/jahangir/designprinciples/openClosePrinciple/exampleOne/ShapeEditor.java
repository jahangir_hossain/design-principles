package com.jahangir.designprinciples.openClosePrinciple.exampleOne;

public class ShapeEditor {
	public void drawShape(Shape shape) {
		shape.draw();
	}

}
