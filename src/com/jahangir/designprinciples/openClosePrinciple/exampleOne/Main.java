package com.jahangir.designprinciples.openClosePrinciple.exampleOne;

public class Main {

	public static void main(String[] args) {
		ShapeEditor editor = new ShapeEditor();
		editor.drawShape(new Circle());
		editor.drawShape(new Rectangle());

	}

}
